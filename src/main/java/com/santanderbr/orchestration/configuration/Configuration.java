package com.santanderbr.orchestration.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.connect.Connectors;
import org.camunda.connect.httpclient.HttpConnector;
import org.camunda.connect.httpclient.HttpResponse;

public class Configuration implements JavaDelegate {

    private static final String URL_API = "https://swapi.co/api/people/1";
    private static final String HEADER = "application/json";

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        HttpConnector http = Connectors.getConnector(HttpConnector.ID);
        HttpResponse httpResponse = http.createRequest().get().url(URL_API).contentType(HEADER).execute();

        String body = httpResponse.getResponse();

        ObjectMapper objectMapper = new ObjectMapper();
        Model model = objectMapper.readValue(body, Model.class);

        System.out.println("Response" + body);
        System.out.println("Response do Objectmappper: " + model.getName());

    }

}